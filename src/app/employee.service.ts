import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }
  public createEmployee(employee: object) {
    return this.http.post('//localhost:8080/create', employee);
  }
  public getEmployeebyId(id: number): Observable<any> {
    return this.http.get<any>('//localhost:8080/employee' + id);
  }
  public getAllEmployees(): Observable<any> {
    return this.http.get<any>('//localhost:8080/employee');
  }
}
