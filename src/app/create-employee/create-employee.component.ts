import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
newEmployee: FormGroup;
  constructor(private service: EmployeeService) { }

  ngOnInit() {
    this.newEmployee = new FormGroup({
      name: new FormControl(),
      salary: new FormControl(),
      position: new FormControl()
    });
  }
  public createEmployee() {
    const Ob = {
      name: this.newEmployee.controls.name.value,
      salary: this.newEmployee.controls.salary.value,
      position: this.newEmployee.controls.position.value
    };
    this.service.createEmployee(Ob).subscribe();
    this.newEmployee.reset();
  }

}
